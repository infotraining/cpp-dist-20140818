#include <iostream>
#include <thread>
#include <mutex>


using namespace std;

class BA
{
    int id_;
    double balance_;
    mutex mtx_;
public:
    BA(int id, double balance) : id_(id), balance_(balance)
    {

    }

    void print()
    {
        cout << "Bank account " << id_ << " balance = " << balance_ << endl;
    }

    void transfer(BA& to, double amount)
    {
        unique_lock<mutex> lg1(mtx_, defer_lock);
        unique_lock<mutex> lg2(to.mtx_, defer_lock);
        lock(lg1, lg2);
        balance_ -= amount;
        to.balance_ += amount;
    }
};

void test_transfer(BA* from, BA* to)
{
    for (int i = 0 ; i < 1000 ; ++i)
        from->transfer(*to, 1.0);
}

int main()
{
    cout << "Hello BA!" << endl;
    BA b1(1, 1000);
    BA b2(2, 1000);
    thread th1(test_transfer, &b1, &b2);
    thread th2(test_transfer, &b2, &b1);
    th1.join();
    th2.join();
    b1.print();
    b2.print();
    return 0;
}

