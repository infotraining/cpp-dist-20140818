#include <iostream>
#include <thread>
#include <vector>
#include <atomic>
#include <mutex>

using namespace std;
mutex mtx;

//atomic<long> counter(0);
long counter = 0;

void worker()
{
    for (int i = 0 ; i < 10000 ; ++i)
    {
        lock_guard<mutex> lg(mtx);
        ++counter;
        //if (counter == 10000)
        //    return;
    }
}

int main()
{
    cout << "Hello counter" << endl;
    vector<thread> thds;

    auto start = std::chrono::high_resolution_clock::now();

    for (int i = 0 ; i < 10 ; ++i)
        thds.emplace_back(&worker);

    for (thread& th : thds) th.join();

    auto end = std::chrono::high_resolution_clock::now();

    cout << "Counter = " << counter << endl;
    cout << "Elapsed: "
         << std::chrono::duration_cast<chrono::microseconds>(end-start).count()
         << " us" << endl;
    return 0;
}

