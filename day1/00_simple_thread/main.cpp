#include <iostream>
#include <thread>
#include <string>
#include <vector>

using namespace std;

void worker(int id, string msg)
{
    cout << "Hello from thread " << id << endl;
    cout << "Message: " << msg << endl;
}

void increment(int& a)
{
    a++;
}

class functor
{
public:
    void operator()()
    {
        cout << "From functor" << endl;
    }
};

std::thread gen_thread()
{
    std::thread th(&worker, 10, "from gen");
    return std::move(th);
}

int main()
{
    cout << "Hello World!" << endl;
    std::thread th1(&worker, 1, "Message");
    int val = 0;
    functor f{};
    std::thread th2(&increment, std::ref(val));
    std::thread th3(f);
    std::thread th4 = gen_thread();
    th1.join();
    th2.join();
    th3.join();
    th4.join();
    cout << "after thread " << val << endl;

    std::vector<std::thread> threads;
    threads.emplace_back(&worker, 2, "dwa");
    threads.emplace_back(&worker, 3, "trzy");
    threads.push_back(gen_thread());
    for (std::thread& th : threads) th.join();

    return 0;
}

