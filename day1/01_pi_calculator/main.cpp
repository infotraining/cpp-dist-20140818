#include <iostream>
#include <random>
#include <chrono>
#include <thread>
#include <algorithm>

using namespace std;

bool in_circle(std::uniform_real_distribution<>& dis, std::mt19937& gen)
{
   float x;
   x = dis(gen);
   float y;
   y = dis(gen);
   return (x*x + y*y) <= 1;
}

void worker(double& pi, long N)
{    
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<> dis(0, 1);
    long counter = 0;
    for (long i = 0 ; i < N ; ++i)
        counter += in_circle(dis, gen);
    pi = 4 * double(counter) / N;
}

int main()
{
    cout << "Hello Pi calc" << endl;
    double pi = 0;
    long N = 10000000;
    int n_of_threads = 4;
    vector<thread> threads;
    vector<double> results(n_of_threads);
    auto start = std::chrono::high_resolution_clock::now();

    for (int i = 0 ; i < n_of_threads ; ++i)
        threads.emplace_back(worker, ref(results[i]), N/n_of_threads);

    for(thread& th : threads) th.join();

    auto end = std::chrono::high_resolution_clock::now();
    pi = accumulate(results.begin(), results.end(), 0.0)/(double)n_of_threads;
    cout << "PI = " << pi << endl;
    cout << "Elapsed: "
         << std::chrono::duration_cast<chrono::milliseconds>(end-start).count()
         << " ms" << endl;
    return 0;
}

