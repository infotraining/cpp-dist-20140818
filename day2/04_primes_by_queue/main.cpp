#include <iostream>
#include "int_queue.h"
#include "thread"
#include <vector>

using namespace std;

bool is_prime(unsigned long v)
{
    for(int d=2; d<v; ++d)
        if( v % d == 0)
            return false;

    return true;
}

int_queue inq("/input_queue");
int_queue outq("/output_queue");

void producer()
{
    for (int i = 10000000; i < 10010000 ; ++i)
        inq.send_message(i);
}

void consumer()
{
    for(;;)
    {
        long num = inq.get_message();
        //cout << "got: " << num << endl;
        if (is_prime(num))
            outq.send_message(num);
    }
}

void sink()
{
    for (;;)
    {
        cout << outq.get_message() << " : " << flush;
    }
}

int main()
{
    mq_unlink("/input_queue");
    mq_unlink("/output_queue");
    vector<thread> threads;
    threads.emplace_back(producer);
    threads.emplace_back(consumer);
    threads.emplace_back(consumer);
    threads.emplace_back(consumer);
    threads.emplace_back(consumer);
    threads.emplace_back(sink);
    for (auto& th : threads) th.join();
    return 0;
}

