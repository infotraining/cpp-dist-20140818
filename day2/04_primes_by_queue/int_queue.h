#ifndef INT_QUEUE_H
#define INT_QUEUE_H

#include <mqueue.h>
#include <string>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

class int_queue
{
    mqd_t id;
public:
    int_queue(const std::string& name){

        id = mq_open(name.c_str(), O_RDWR | O_CREAT, 0666, NULL);
        if (id == -1) {
            perror("in creating");
            throw std::exception();
        }
    }

    void send_message(long msg)
    {
        mq_send(id, (char*)&msg, sizeof(msg), 1);
    }

    long get_message()
    {
        long res;
        char buff[8192];
        int size = mq_receive(id, buff, 8192, NULL);
        memcpy(&res, buff, sizeof(int));
        if (size == -1)
        {
            perror("in creating");
            throw std::exception();
        }
        return res;
    }

    ~int_queue()
    {
        mq_close(id);
    }
};

#endif // INT_QUEUE_H
