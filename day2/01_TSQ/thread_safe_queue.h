#ifndef THREAD_SAFE_QUEUE_H
#define THREAD_SAFE_QUEUE_H
#include <mutex>
#include <condition_variable>
#include <queue>

template<class T>
class thread_safe_queue
{
    std::mutex mtx;
    std::condition_variable cnd;
    std::queue<T> q;

public:
    thread_safe_queue()
    {

    }

    void push(T item)
    {
        std::lock_guard<std::mutex> g(mtx);
        q.push(item);
        cnd.notify_one();
    }

    void pop(T& item)
    {
        std::unique_lock<std::mutex> g(mtx);
        cnd.wait(g, [this] { return !q.empty();});
        item = q.front();
        q.pop();
    }

    bool try_pop(T& item)
    {
        std::lock_guard<std::mutex> g(mtx);
        if (q.empty())
            return false;
        item = q.front();
        q.pop();
        return true;
    }

};

#endif // THREAD_SAFE_QUEUE_H
