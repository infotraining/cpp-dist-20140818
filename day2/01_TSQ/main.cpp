#include <iostream>
#include <thread>
#include "thread_safe_queue.h"

using namespace std;

thread_safe_queue<int> q;

void producer()
{
    for (int i = 0 ; i < 100 ; ++i)
    {
        q.push(i);
        this_thread::sleep_for(chrono::milliseconds(10));
    }
}

void consumer(int id)
{
    for(;;)
    {
        int msg;
        q.pop(msg);
        cout << "msg got by " << id << " : " << msg << endl;
    }
}

int main()
{
    cout << "Hello World!" << endl;
    thread prod(&producer);
    thread cons1(&consumer, 1);
    thread cons2(&consumer, 2);
    prod.join();
    cons1.join();
    cons2.join();
    return 0;
}
