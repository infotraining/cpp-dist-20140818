#include <iostream>
#include <thread>
#include <queue>
#include <mutex>
#include <condition_variable>

using namespace std;

queue<int> q;
mutex mtx;
condition_variable cond;

void producer()
{
    for (int i = 1 ; i <= 100 ; ++i)
    {
        this_thread::sleep_for(chrono::milliseconds(100));
        {
            lock_guard<mutex> g(mtx);
            q.push(i);
            cond.notify_one();
        }
    }
    q.push(0);
    cond.notify_one();
}

void consumer(int id)
{
    for(;;)
    {

        unique_lock<mutex> g(mtx);
//        while (q.empty())
//        {
//            cond.wait(g);
//        }
        cond.wait(g, [] {return !q.empty();});
        int msg = q.front();
        if (msg == 0)
        {
            cond.notify_one();
            return;
        }
        q.pop();
        cout << "msg got by " << id << " : " << msg << endl;

    }
}

int main()
{
    cout << "Hello World!" << endl;
    thread prod(&producer);
    thread cons1(&consumer, 1);
    thread cons2(&consumer, 2);
    prod.join();
    cons1.join();
    cons2.join();
    return 0;
}

