#include <iostream>
#include <functional>
#include "thread_pool.h"

using namespace std;

void process()
{
    cout << "PRocess" << endl;
}

int main()
{
    thread_pool tp(2);
    tp.submit_task(process);
    tp.submit_task([] { cout << "Hello from lambda" << endl;});
    for (int i = 0 ; i < 10 ; ++i)
    {
        tp.submit_task( [i] {
            this_thread::sleep_for(chrono::milliseconds(100));
            cout << "Task id " << i << endl;});
    }
    cout << "Hello World!" << endl;
    return 0;
}

