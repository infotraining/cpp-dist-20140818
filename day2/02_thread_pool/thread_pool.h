#ifndef THREAD_POOL_H
#define THREAD_POOL_H
#include <functional>
#include "thread_safe_queue.h"
#include <iostream>
#include <vector>
#include <thread>

typedef std::function<void()> Task;

class thread_pool
{
    thread_safe_queue<Task> q;
    std::vector<std::thread> workers;
public:
    thread_pool(int size)
    {
        for (int i = 0 ; i < size ; ++i)
            workers.emplace_back(&thread_pool::worker, this);

    }
    void submit_task(Task task)
    {
        q.push(task);
    }
    ~thread_pool()
    {
        for (auto& th : workers) q.push(nullptr);
        for (auto& th : workers) th.join();
    }

    void worker()
    {
        for(;;)
        {
            Task t;
            q.pop(t);
            if (!t) return;
            t();
            std::cout << "By: " << std::this_thread::get_id() << std::endl;
        }
    }

};

#endif // THREAD_POOL_H
