#include <iostream>
#include <mqueue.h>
#include <string>
#include <string.h>

using namespace std;

int main()
{
    cout << "Hello queue" << endl;
    mqd_t id = mq_open("/myqueue1", O_RDWR | O_CREAT, 0666, NULL);
    if (id == -1) {
        cerr << "Error opening queue" << endl;
        return 0;
    }
    struct mq_attr attrs;
    mq_getattr(id, &attrs);
    cout << "queue: " << attrs.mq_maxmsg << " " << attrs.mq_msgsize << endl;

    char buff[8192];
    int size = mq_receive(id, buff, sizeof(buff), NULL);
    if (size == -1)
    {
        cerr << "Error in read" << endl;
    }
    cout << buff << endl;
}
