#include <iostream>
#include "../zeromq/zhelpers.hpp"
#include "../zeromq/zmq.hpp"
#include <string>
#include <thread>

using namespace std;

int main()
{
    cout << "Renderer server" << endl;
    zmq::context_t context(1);
    zmq::socket_t socket(context, ZMQ_ROUTER);
    socket.bind("tcp://*:5556");
//    std::cout << "Press Enter when the workers are ready: " << std::endl;
//    getchar ();
//    std::cout << "Sending tasks to workers...\n" << std::endl;

    for (int line = 0 ; line < 768 ; ++line)
    {
        std::string address = s_recv(socket);
        cout << "addres of client" << address << endl;
        cout << "client id: " << s_recv(socket);
        cout << s_recv(socket) << endl;
        s_sendmore(socket, address);
        s_sendmore(socket, "");
        cout << "just send " << line << endl;
        s_send(socket, to_string(line));
        this_thread::sleep_for(chrono::milliseconds(200));
    }
    return 0;
}

