#include <iostream>
#include "../zeromq/zhelpers.hpp"
#include "../zeromq/zmq.hpp"

using namespace std;

int main()
{
    cout << "ZMQ client" << endl;
    zmq::context_t context(1);
    zmq::socket_t socket(context, ZMQ_REQ);
    socket.connect("tcp://127.0.0.1:5555");
    for (int i = 0 ; i < 5 ; ++i)
    {
        s_send(socket, "Good day for you");
        cout << "From server " << s_recv(socket) << endl;

    }
    return 0;
}
