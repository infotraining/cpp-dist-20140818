#include <iostream>
#include <SimpleAmqpClient/SimpleAmqpClient.h>
#include <string>
#include <thread>

using namespace std;
using namespace AmqpClient;

string exchange_name = "Nokia-test";
//string q_name = "Nokia-queue";

void send()
{
    Channel::ptr_t channel_in;
    channel_in = Channel::Create("192.168.1.3", 5672,
                                 "admin", "tymczasowe");
    std::string uid = "[ Leszek ] : ";
    while(true)
    {
        std::string msg;
        cout << ">>> ";
        std::getline(std::cin, msg);
        msg = uid + msg;
        channel_in->BasicPublish(exchange_name, "chat", BasicMessage::Create(msg));
    }
}

void receive()
{
    Channel::ptr_t channel_out;
    channel_out = Channel::Create("192.168.1.3", 5672,
                                  "admin", "tymczasowe");
    string q_name = channel_out->DeclareQueue("");
    channel_out->BindQueue(q_name, exchange_name);
    channel_out->BasicConsume(q_name);
    while(true)
    {
        BasicMessage::ptr_t msg = channel_out->BasicConsumeMessage()->Message();
        cout << ". " << msg->Body() << endl;
    }
}

int main()
{
    thread th1(&send);
    thread th2(&receive);
    th2.join();
    th1.join();
}
