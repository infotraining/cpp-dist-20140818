#include <iostream>
#include <SimpleAmqpClient/SimpleAmqpClient.h>
#include <string>

using namespace std;
using namespace AmqpClient;

string exchange_name = "Nokia-test";
string q_name = "Nokia-queue";

void send()
{
    Channel::ptr_t channel_in;
    channel_in = Channel::Create();
    channel_in->DeclareExchange(exchange_name);
    channel_in->DeclareQueue(q_name, false, true, false, false);
    channel_in->BindQueue(q_name, exchange_name, "chat");
    channel_in->BasicPublish(exchange_name, "chat",
                             BasicMessage::Create("medium is the message"));

}

void recieve()
{
    Channel::ptr_t channel_out;
    channel_out = Channel::Create();
    channel_out->BasicConsume(q_name);
    while(true)
    {
        BasicMessage::ptr_t msg = channel_out->BasicConsumeMessage()->Message();
        cout << msg->Body() << endl;
    }
}

int main()
{
    send();
    recieve();
}

