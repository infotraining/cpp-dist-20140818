#include <iostream>
#include <SimpleAmqpClient/SimpleAmqpClient.h>
#include <string>

using namespace std;
using namespace AmqpClient;

string exchange_name = "Nokia-test";
string q_name = "Nokia-queue";


void setup()
{
    Channel::ptr_t channel_in;
    channel_in = Channel::Create("192.168.1.3", 5672,
                                 "admin", "tymczasowe");
    channel_in->DeclareExchange(exchange_name, Channel::EXCHANGE_TYPE_FANOUT,
                                false, true, false);
    //channel_in->DeclareQueue(q_name, false, true, false, false);
    //channel_in->BindQueue(q_name, exchange_name, "chat");
}

void recieve()
{
    Channel::ptr_t channel_out;
    channel_out = Channel::Create();
    channel_out->BasicConsume(q_name);
    while(true)
    {
        BasicMessage::ptr_t msg = channel_out->BasicConsumeMessage()->Message();
        cout << msg->Body() << endl;
    }
}


int main()
{
    cout << "Hello Server" << endl;
    //recieve();
    setup();
    return 0;
}

